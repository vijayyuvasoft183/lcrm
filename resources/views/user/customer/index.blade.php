@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')

    <div class="page-header clearfix">
        @if($user->hasAccess(['customers.write']) || $orgRole=='admin')
            <div class="pull-right">
               <!--  <a href="{{ $type.'/create' }}" class="btn btn-primary m-b-10">
                    <i class="fa fa-plus-circle"></i> {{ trans('customer.create') }}</a>
                <a href="{{ $type.'/import' }}" class="btn btn-primary m-b-10">
                    <i class="fa fa-download"></i> {{ trans('customer.import') }}</a> -->
            </div>
        @endif
    </div>

    <div class="card">
        <div class="card-header bg-white">
            <h4 class="float-left">
                <i class="material-icons">flag</i>
                {{ $title }}
            </h4>
            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removecard clickable"></i>
                                </span>
        </div>
        <div class="card-body ">
            <div class="table-responsive">

                <table class="table table-striped table-bordered ">
                    <thead>
                    <tr>
                        <th>{{ trans('customer.full_name') }}</th>  
                        <th>{{ trans('customer.phone') }}</th>
                        <th>{{ trans('table.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($customers as $customer)
                        <tr>
                            <td>{{ $customer->title}} {{ $customer->name }}</td>   
                            <td>{{ $customer->mobile}}</td>
                            <td><a href="{{ url('customer/' . $customer->id . '/show' ) }}" title="{{ trans('table.details') }}" ><i class="fa fa-fw fa-eye text-primary"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  
@stop

{{-- Scripts --}}
@section('scripts')
    <script src="{{ asset('js/todolist.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
    <script>
        $(document).ready(function () {
            $("#user_id").select2({
                theme: "bootstrap",
                placeholder: "{{trans('task.user')}}"
            });

            var dateFormat = '{{ config('settings.date_format') }}';
            flatpickr("#task_deadline", {
                minDate: '{{  now() }}',
                dateFormat: dateFormat,
            });

            $('#calendar').fullCalendar({
                "header": {
                    "left": "prev,next today",
                    "center": "title",
                    "right": "month,agendaWeek,agendaDay"
                },
                "eventLimit": true,
                "firstDay": 1,
                "eventClick": function(event){
                    $('#modalTitle').html(event.title);
                    $('#modalBody').html(event.description);
                    $('#fullCalModal').modal();
                },
                "events": [
                    @foreach($appointments as $appointment)
                    {
                        title : '{{ $appointment->title }}',
                        start : '{{ $appointment->assign_date }}',
                    },
                    @endforeach
                ],
                "eventSources": [
                    {
                        url:"{{url('calendar/events')}}",
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}'
                        },
                        error: function() {
                            alert('there was an error while fetching events!');
                        }
                    }
                ]
            });
        });
        $('.icheckgreen').iCheck({
            checkboxClass: 'icheckbox_minimal-green',
            radioClass: 'iradio_minimal-green'
        });
    </script>
    <!-- Scripts -->
    @if(isset($type))
        <script type="text/javascript">
            var oTable;
            $(document).ready(function () {
                oTable = $('#data').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "columns":[
                        {"data":"full_name"},
                        {"data":"company"},
                        {"data":"email"},
                        {"data":"phone"},
                        {"data":"actions"}
                    ],
                    "ajax": "{{ url($type) }}" + ((typeof $('#data').attr('data-id') != "undefined") ? "/" + $('#id').val() + "/" + $('#data').attr('data-id') : "/data")
                });
                $('div.dataTables_length select').select2({
                    theme:"bootstrap"
                });
            });
        </script>
    @endif
@stop
