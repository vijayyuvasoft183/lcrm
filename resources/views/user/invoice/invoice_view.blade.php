@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')

	<div class="card">
      <div class="card-header bg-white">
        <h4 class="float-left">
          <i class="material-icons">thumb_up</i>
          {{ $title }}
        </h4>
        <span class="pull-right">
          <i class="fa fa-fw fa-chevron-up clickable"></i>
          <i class="fa fa-fw fa-times removecard clickable"></i>
        </span>
      </div>
      <div class="card-body">

        <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Invoice Number</th>
                <th>Invoice Date</th>
                <th>Customer Name</th>
                <th>Total</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
          @foreach($invoiceDatas as $key => $invoice)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{ $invoice->invoice_number }}</td>
                <td>{{ $invoice->invoice_date}}</td>
                <td>{{ $invoice->customerName->name}}</td>
                <td>{{ $invoice->total }}</td>
                <td><a href="{{ url('invoice/' . $invoice->id . '/display' ) }}" title="{{ trans('table.details') }}" ><i class="fa fa-fw fa-eye text-primary"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
      </div>
    </div>	
@stop