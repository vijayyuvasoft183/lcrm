@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="page-header clearfix">
            </div>
            <div class="details">
        		<div class="card">
				    <div class="card-body">
				        <div class="row">
				            <div class="col-md-12">
				                {{--logged_calls--}}
				                @if($user->hasAccess(['logged_calls.read']))
				                    <a href="{{ url('leadcall/' . $lead->id ) }}" class="btn btn-primary call-summary">
				                        <i class="fa fa-phone"></i> <b>{{$lead->calls()->count()}}</b> {{ trans("table.calls") }}
				                    </a>
				                    @endif
				            </div>
				        </div>

				        <div class="row">
				        	<div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('invoice_number', trans('invoice.invoice_number'), ['class' => 'control-label']) !!}
				                    <div>{{ $invoiceData->invoice_number }}</div>
				                </div>
				            </div>
				             <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('invoice_date', trans('invoice.invoice_date'), ['class' => 'control-label']) !!}
				                    <div>{{ $invoiceData->invoice_date }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('grand_total', trans('invoice.grand_total'), ['class' => 'control-label']) !!}
				                    <div>{{ $invoiceData->grand_total }}</div>
				                </div>
				            </div>
				        	<div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('property_type', trans('invoice.property_type'), ['class' => 'control-label']) !!}
				                    <div>{{ $ordersData->property_type }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('roof_type', trans('invoice.roof_type'), ['class' => 'control-label', 'placeholder'=>'select']) !!}
				                    <div>{{ $ordersData->roof_type }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('roof_condition', trans('invoice.roof_condition'), ['class' => 'control-label' ]) !!}
				                    <div><?php $roof_conditionData = unserialize($ordersData->roof_condition) ;?>
											@foreach($roof_conditionData as $key => $conditions )
												@if($key == "ridge")  Ridge Tiles : {{$conditions}} @endif
												@if($key == "hip")  Hip/Ends : {{$conditions}} @endif
												@if($key == "repair_ridge")  Repair Ridge : {{$conditions}} @endif
												@if($key == "repair_hip")  Repair Hip/Ends : {{$conditions}} @endif
												<br/>
											@endforeach
									</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('tile_condition', trans('invoice.tile_condition'), ['class' => 'control-label']) !!}
				                    <div>
				                    	<?php $tile_conditionData = unserialize($ordersData->tile_condition); ?>
										@foreach($tile_conditionData as $key => $conditions )
											@if($key == "moss")  Moss : {{$conditions}} @endif
											@if($key == "fungi")  Fungi : {{$conditions}} @endif
											@if($key == "lichen")  Lichen : {{$conditions}} @endif
											<br/>
										@endforeach 
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('wall_surface', trans('invoice.wall_surface'), ['class' => 'control-label']) !!}
				                    <div>{{ $ordersData->wall_surface ?? null }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('notes', trans('invoice.notes'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->notes) ? "Not Available" : $ordersData->notes }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('masonry_creme', trans('invoice.masonry_creme'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->masonry_creme) ? "Not Available" : $ordersData->masonry_creme  }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('coloured_facade', trans('invoice.coloured_facade'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->coloured_facade) ? "Not Available" : $ordersData->coloured_facade }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('water_repellant', trans('invoice.water_repellant'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->water_repellant) ? "Not Available" : $ordersData->water_repellant }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('renotec_system', trans('invoice.renotec_system'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->renotec_system) ? "Not Available" : $ordersData->renotec_system }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('driveway', trans('invoice.driveway'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->driveway) ? "Not Available" : $ordersData->driveway }}</div>
				                </div>
				            </div>
				             <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('colour_coating', trans('invoice.colour_coating'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->colour_coating) ? "Not Available" : $ordersData->colour_coating }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('tile_colour', trans('invoice.tile_colour'), ['class' => 'control-label']) !!}
				                   	<div>{{ empty($ordersData->tile_colour) ? "Not Available" : $ordersData->tile_colour }}</div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('proparation_work', trans('invoice.proparation_work'), ['class' => 'control-label']) !!}
				                    <div>
				                    	<?php $proparation_work = unserialize($ordersData->proparation_work); ?>
										@foreach($proparation_work as $key => $conditions )
											{{$conditions}} <br/>
										@endforeach
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('excess_equipement', trans('invoice.excess_equipement'), ['class' => 'control-label']) !!}
				                    <div>
				                    	<?php $excess_equipement = unserialize($ordersData->excess_equipement); ?>
										@foreach($excess_equipement as $key => $conditions )
											{{$conditions}} <br/>
										@endforeach 
				                    </div>
				                </div>
				            </div>
				            <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('moss_clean', trans('invoice.moss_clean'), ['class' => 'control-label']) !!}
				                    <div>
				                    	<?php $moss_cleanData = unserialize($ordersData->moss_clean); ?>
										@foreach($moss_cleanData as $key => $conditions )
											@if($key == "house_roof") House Roof : {{$conditions}} @endif 
											@if($key == "garage_roof") Garage Roof : {{$conditions}} @endif
											@if($key == "out_building_moss") OutBuilding : {{$conditions}} @endif
											<br/>
										@endforeach 
				                    </div>
				                </div>
				            </div>
				             <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('gutter_downpipe', trans('invoice.gutter_downpipe'), ['class' => 'control-label']) !!}
				                    <div>
				                    	<?php $gutter_downpipe = unserialize($ordersData->gutter_downpipe);?>
										@foreach($gutter_downpipe as $key => $conditions )
											@if($key == "gutter_clean") Full Gutter Clean/Clear : {{$conditions}} @endif
											@if($key == "downpipe_clean") Downpipe Flush : {{$conditions}} @endif<br/>
										@endforeach
				                    </div>
				                </div>
				            </div>
				             <div class="col-sm-6 col-lg-3">
				                <div class="form-group">
				                    {!! Form::label('surveyors_note', trans('invoice.surveyors_note'), ['class' => 'control-label']) !!}
				                    <div>{{ empty($ordersData->surveyors_note) ? "Not Available" : $ordersData->surveyors_note }}</div>
				                </div>
				            </div>
				        </div>
				        <div class="form-group">
				            <div class="controls">
				                <a href="{{ url('invoice_view') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> {{trans('table.back')}}</a>
				            </div>
				        </div>
				    </div>
				</div>	
            </div>
        </div>
    </div>
@stop