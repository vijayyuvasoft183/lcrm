@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop
{{-- Content --}}
@section('content')

<div class="container">
  
<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Step 3</p>
        </div>
    </div>
</div>
  
{{ Form::open(array( 'url'=> 'order_create') )}}
    <div class="row setup-content" id="step-1">
        <input type="hidden" name="lead_id" value="<?php echo $_GET['id'] ?>">
        <div class="col-md-6">
            <div class="row">
                <!-- Start col-md-6 -->
                <div class="col-md-6">
                    <div class="card property-type">
                        <div class="card-header bg-white">
                          <h5 class="float-left">
                             Property Type
                          </h5>
                        </div>
                        <div class="card-body">
                            <label class="custom-radio"> Detached
                                <input type="radio" value="detached" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Semi Detached
                                <input type="radio" value="semi_detached" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Bungalow
                                <input type="radio"  value="bungalow" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Mobile Home
                                <input type="radio"  value="mobile_home" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> OutBuilding
                                <input type="radio"  value="out_building" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Fat
                                <input type="radio"  value="fat" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Apartment
                                <input type="radio"  value="apartment" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Terraced
                                <input type="radio"  value="terraced" name="property_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="custom-radio"> Mid
                                        <input type="radio"  value="mid" name="terraced"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="custom-radio"> End
                                        <input type="radio"  value="end" name="terraced"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>        
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header bg-white">
                          <h5 class="float-left">
                             Roof Type
                          </h5>
                        </div>
                        <div class="card-body roof-type">
                            <label class="custom-radio"> Tile
                                <input type="radio"  value="tile" name="roof_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Slate
                                <input type="radio"  value="slate" name="roof_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Pantile
                                <input type="radio"  value="pantile" name="roof_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Steel
                                <input type="radio"  value="steel" name="roof_type"> 
                                <span class="checkmark"></span>
                            </label>
                            <label class="custom-radio"> Asphalt
                                <input type="radio"  value="asphalt" name="roof_type"> 
                                <span class="checkmark"></span>
                            </label>   
                        </div>
                    </div>
                </div>
                <!-- Start col-md-6 -->   
                <div class="col-md-6">
                    <div class="card wall_condition">
                        <div class="card-header bg-white">
                          <h5 class="float-left">
                             Roof Wall Condition
                          </h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <p>Ridge Tiles</p>
                                <label class="custom-radio"> Good
                                    <input type="radio"  value="good" checked="checked" name="ridge" required> 
                                    <span class="checkmark"></span>
                                </label>
                                <label class="custom-radio"> Fair
                                    <input type="radio"  value="fair" name="ridge"> 
                                    <span class="checkmark"></span>
                                </label>
                                <label class="custom-radio"> Bad
                                    <input type="radio"  value="bad" name="ridge"> 
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-group">
                                <p>Hip/Ends</p>
                                <label class="custom-radio"> Good
                                    <input type="radio"  value="good" checked="checked" name="hip" required> 
                                    <span class="checkmark"></span>
                                </label>
                                <label class="custom-radio"> Fair
                                    <input type="radio"  value="fair" name="hip"> 
                                    <span class="checkmark"></span>
                                </label>
                                <label class="custom-radio"> Bad
                                    <input type="radio"  value="bad" name="hip"> 
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-group">
                                <p>Repair Ridge</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="custom-radio"> Yes
                                            <input type="radio"  value="yes" name="repair_ridge" required> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">    
                                        <label class="custom-radio"> No
                                            <input type="radio"  value="no" checked="checked" name="repair_ridge"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>    
                                </div>    
                            </div>
                            <div class="form-group">
                                <p>Repair Hip/Ends</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="custom-radio"> Yes
                                            <input type="radio"  value="yes" checked="checked" name="repair_hip" required> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">    
                                        <label class="custom-radio"> No
                                            <input type="radio"  value="no" checked="checked" name="repair_hip"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>    
                                </div>    
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white">
                          <h5 class="float-left">
                             Tile Condition
                          </h5>
                        </div>
                        <div class="card-body tile-condition">
                            <div class="form-group">
                                <p>Moss</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="custom-radio"> Yes
                                            <input type="radio"  value="yes" name="moss"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">    
                                        <label class="custom-radio"> No
                                            <input type="radio"  value="no" name="moss" checked="checked"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>    
                                </div>    
                            </div>
                            <div class="form-group">
                                <p>Fungi</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="custom-radio"> Yes
                                            <input type="radio"  value="yes" name="fungi"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">    
                                        <label class="custom-radio"> No
                                            <input type="radio"  value="no" name="fungi" checked="checked"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>    
                                </div>    
                            </div>
                            <div class="form-group">
                                <p>Lichen</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="custom-radio"> Yes
                                            <input type="radio"  value="yes" name="lichen"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">    
                                        <label class="custom-radio"> No
                                            <input type="radio"  value="no" name="lichen" checked="checked"> 
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>    
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
        
        <div class="col-md-6">  
            <div class="card">
                <div class="card-header bg-white">
                  <h5 class="float-left">
                     Wall Surface
                  </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="custom-radio"> Brick
                                <input type="radio"  value="brick" name="wall_surface"> 
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="custom-radio"> Render
                                <input type="radio"  value="render" name="wall_surface"> 
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="custom-radio"> Pebble Dash
                                <input type="radio"  value="pebble" name="wall_surface"> 
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="custom-radio"> Other
                                <input type="radio"  value="other" name="wall_surface"> 
                                <span class="checkmark"></span>
                            </label>
                        </div>    
                    </div>    
                    <!-- Divider -->
                    <hr/>
                    <div class="wall-surface-input form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-user blue-back"></i> Properla Masonry Creme
                            </div>
                            <div class="col-md-5">
                                <input type="text" name="masonry_creme" class="form-control" placeholder="Input Value">
                            </div>
                        </div>    
                    </div>
                    <div class="wall-surface-input form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-user blue-back"></i> Properla Coloured Facade
                            </div>
                            <div class="col-md-5">
                                <input type="text" name="coloured_facade" class="form-control" placeholder="Input Value">
                            </div>
                        </div>    
                    </div>
                    <div class="wall-surface-input form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-user blue-back"></i> Properla Water Repellant
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="water_repellant" placeholder="Input Value">
                            </div>
                        </div>    
                    </div>
                    <div class="wall-surface-input form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-user blue-back"></i> Properla Renotec System
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="renotec_system" placeholder="Input Value">
                            </div>
                        </div>    
                    </div>
                    <div class="wall-surface-input form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-user blue-back"></i> Properla Driveway impregnato
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="driveway" placeholder="Input Value">
                            </div>
                        </div>    
                    </div>
                    <div class="wall-surface-input form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-user blue-back"></i> Facade Colour Coating
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="colour_coating" placeholder="Input Value">
                            </div>
                        </div>    
                    </div>
                     <div class="wall-surface-input form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-user blue-back"></i> Renotec Tile Colour
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="tile_colour" placeholder="Input Value">
                            </div>
                        </div>    
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header bg-white">
                  <h5 class="float-left">
                     Photos
                  </h5>
                </div>
                <div class="card-body">
                    <div class="photo-area">
                        <h5 class="font-weight-bold">Photos of application area are required</h5>
                        <p>This order will not be processed without photos of all elevations prior to install</p>
                        <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
                              <button type="button" class="btn btn-secondary bg-dark" name="cancel">Cancel</button>
                              <button type="button" class="btn btn-primary" name="ok">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Start col-md-12 -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-white">
                  <h5 class="float-left">
                     Note
                  </h5>
                </div>
                <textarea class="card-body" name="notes">
                  
                </textarea>
            </div>
        </div>         
        <div class="col-md-12">
            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>       
    </div>

    <div class="row setup-content" id="step-2">
            <div class="col-md-8">
                <div class="row">
                    <!-- Start col-md-6 -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header bg-white">
                              <h5 class="float-left">
                                 Proparation Work
                              </h5>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="custom-checkbox">Roof Retile
                                        <input type="checkbox" value="roof_retile" name="proparation_work[]" checked="checked" checked="checked"> <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="custom-checkbox">Tile Replace/fix
                                        <input type="checkbox"  value="tile_replace" name="proparation_work[]"> <span class="checkmark"></span>
                                    </label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select class="form-control">
                                                <option>Replacements</option>
                                            </select>
                                        </div> 
                                        <div class="col-md-6">
                                             <select class="form-control">
                                                <option>Fixes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                        <label class="custom-checkbox">Pointing
                                            <input type="checkbox"  value="pointing" name="proparation_work[]"> <span class="checkmark"></span>
                                        </label>      
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <input type="text" name="proparation_work[]" class="form-control" placeholder="Ridge Area"> 
                                                <div class="input-group-append"><span class="input-group-text">m2</span></div>
                                            </div> 
                                        </div>
                                    </div>  
                                </div>
                                <div class="form-group">      
                                    <div class="row">
                                        <div class="col-md-6">
                                        <label class="custom-checkbox">Pointing
                                            <input type="checkbox"  value="pointing_hip" name="proparation_work[]"> <span class="checkmark"></span>
                                        </label>      
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <input type="text" name="proparation_work[]" class="form-control" placeholder="Hip/end area" > 
                                                <div class="input-group-append"><span class="input-group-text">m2</span></div>
                                            </div> 
                                        </div>
                                    </div>   
                                </div>
                                <div class="form-group">     
                                    <label class="custom-checkbox">External Roof Work
                                        <input type="checkbox"  value="external_roof" name="proparation_work[]"> <span class="checkmark"></span>
                                    </label>  
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="custom-checkbox">Ridge tile replacement
                                                <input type="checkbox"  value="ridge_tile_replacement" name="proparation_work[]"> <span class="checkmark"></span>
                                            </label>         
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="custom-checkbox">Lead flashing replacement
                                                <input type="checkbox"  value="lead_flash_replacement" name="proparation_work[]"> <span class="checkmark"></span>
                                            </label>     
                                        </div>
                                    </div>  
                                </div>
                                <div class="form-group">  
                                    <div class="row">
                                        <div class="col-md-12">
                                             <label class="custom-checkbox">Valley tile replacement
                                                <input type="checkbox"  value="valley_tile_replacement" name="proparation_work[]"> <span class="checkmark"></span>
                                            </label> 
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">  
                                    <div class="row">
                                        <div class="col-md-12">
                                             <label class="custom-checkbox">Lead valley replacement
                                                <input type="checkbox"  value="lead_valley_replacement" name="proparation_work[]"> <span class="checkmark"></span>
                                            </label> 
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">    
                                    <div class="row">
                                        <div class="col-md-12">
                                        <label class="custom-checkbox">Cement Work
                                            <input type="checkbox"  value="cement_work" name="proparation_work[]"> <span class="checkmark"></span>
                                        </label>      
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group mb-3">
                                                <input type="text" name="proparation_work[]" class="form-control" placeholder="patch" > 
                                                <div class="input-group-append"><span class="input-group-text">m2</span></div>
                                            </div> 
                                        </div>
                                    </div> 
                                </div>          
                            </div>
                        </div>
                    </div>
                    <!-- Start col-md-6 -->   
                    <div class="col-md-6">
                        <div class="card excess_equipement">
                            <div class="card-header bg-white">
                              <h5 class="float-left">
                                Excess Equipment
                              </h5>
                            </div>
                            <div class="card-body">
                                <p>Scaffolding</p>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="custom-checkbox">Front
                                                <input type="checkbox"  value="front" name="excess_equipement[]" checked="checked"> <span class="checkmark"></span>
                                            </label>      
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control">
                                                <option>lifts</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="custom-checkbox">Rear
                                                <input type="checkbox"  value="rear" name="excess_equipement[]"> <span class="checkmark"></span>
                                            </label>      
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control">
                                                <option>lifts</option>
                                            </select>
                                        </div>
                                    </div>     
                                </div>    
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="custom-checkbox">Left Side
                                                <input type="checkbox"  value="left_side" name="excess_equipement[]"> <span class="checkmark"></span>
                                            </label>      
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control">
                                                <option>lifts</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-group">       
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="custom-checkbox">Right Side
                                                <input type="checkbox"  value="right_side" name="excess_equipement[]"> <span class="checkmark"></span>
                                            </label>      
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control">
                                                <option>lifts</option>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="custom-checkbox">Ladders
                                                <input type="checkbox"  value="ladders" name="excess_equipement[]"> <span class="checkmark"></span>
                                            </label>      
                                        </div>
                                        <div class="col-md-6">
                                            <label class="custom-checkbox">Towers
                                                <input type="checkbox"  value="towers" name="excess_equipement[]"> <span class="checkmark"></span>
                                            </label>   
                                        </div>
                                    </div>
                                </div>                                          
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        
            <div class="col-md-4">  
                <div class="card">
                    <div class="card-header bg-white">
                      <h5 class="float-left">
                         Moss Clean/Clean/Anti Fungal wash
                      </h5>
                    </div>
                    <div class="card-body">
                         <div class="form-group">
                            <p>House Roof</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="custom-radio"> Yes
                                        <input type="radio"  value="yes" name="house_roof"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-6">    
                                    <label class="custom-radio"> No
                                        <input type="radio"  value="no" name="house_roof" checked="checked"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>    
                            </div>    
                        </div>
                         <div class="form-group">
                            <p>Garage Roof</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="custom-radio"> Yes
                                        <input type="radio"  value="yes" name="garage_roof"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-6">    
                                    <label class="custom-radio"> No
                                        <input type="radio"  value="no" name="garage_roof" checked="checked"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>    
                            </div>    
                        </div>
                         <div class="form-group">
                            <p>OutBuilding</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="custom-radio"> Yes
                                        <input type="radio"  value="yes" name="out_building_moss"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-6">    
                                    <label class="custom-radio"> No
                                        <input type="radio"  value="no" name="out_building_moss" checked="checked"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>    
                            </div>    
                        </div>   
                    </div>
                </div>

                <div class="card gutter">
                    <div class="card-header bg-white">
                      <h5 class="float-left">
                        Gutter & Downpipe
                      </h5>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <p>Full Gutter Clean/Clear</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="custom-radio"> Yes
                                        <input type="radio"  value="yes" name="gutter"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-6">    
                                    <label class="custom-radio"> No
                                        <input type="radio"  value="no" name="gutter" checked="checked"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>    
                            </div>    
                        </div>
                        <div class="form-group">
                            <p>Downpipe Flush</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="custom-radio"> Yes
                                        <input type="radio"  value="yes" name="downpipe"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-6">    
                                    <label class="custom-radio"> No
                                        <input type="radio"  value="no" name="downpipe" checked="checked"> 
                                        <span class="checkmark"></span>
                                    </label>
                                </div>    
                            </div>    
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header bg-white">
                              <h5 class="float-left">
                                Surveyors Note
                              </h5>
                            </div>
                            <div class="card-body">
                              <textarea style="width: 606px;height: 736px;" name="surveyors_note"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header bg-white">
                              <h5 class="float-left">
                                 Terms & Conditions
                              </h5>
                            </div>
                            <div class="card-body">
                               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                               <br/><br/>
                               Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>  
    </div>

    <div class="row setup-content" id="step-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5>Roof Plan</h5>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <button class="btn btn-primary nextBtn btn-lg pull-right" type="submit" >Submit</button>
        </div>  
    </div>
{{ Form::close() }}
  
</div>
@stop

{{-- Scripts --}}
@section('scripts')
    <script>
        $(document).ready(function () {
            var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                  $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

                $(".form-group").removeClass("has-error");
                for(var i=0; i<curInputs.length; i++){
                    if (!curInputs[i].validity.valid){
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        });
    </script>    

@stop
<style type="text/css">
    body {
    margin-top:40px;
}
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-step a{
    border-radius: 50%;
    -webkit-appearance: none !important;
    border: 2px solid #3295ff;
    background: #fff;
    height: 30px;
    width: 30px;
    line-height: 28px;
    text-align: center;
    padding: 0;
    padding-left: 2px;
    display: block;
} 
.stepwizard-step a.btn-primary{
    background: #3295ff;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    margin: 0 auto;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 70%;
    height: 1px;
    background-color: #3295ff;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.wall-surface-input{
    margin-top: 19.5px;
}
.wall-surface-input i{
    height: 28px;
    width: 28px;
    border-radius: 50%;
    line-height: 28px;
    text-align: center;
    margin-right: 8px;
}
.blue-back{
    background: blue;
    color: #fff;
}
.photo-area{
    width: 75%;
    margin: 55 auto;
}
.photo-area .btn{
    width: 100px;
}
.photo-area h5{
    color: #444;
}
.photo-area p{
    color: #777;
}
.property-type{
    height: 591px;
}
.roof-type{
    height: 296px;
}
.excess_equipement{
    height: 700px;
}
.gutter{
    height: 298px;
}
.wall_condition{
    height: 591px;
}
</style>