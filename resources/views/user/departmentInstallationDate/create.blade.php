@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-6">
            <div class="card todolist">
                <div class="card-header bg-white">
                    <h4 class="float-left">
                        <i class="livicon" data-name="medal" data-size="18" data-color="white" data-hc="white"
                           data-l="true"></i>
                        {{trans('installation.create')}}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="todolist_list adds">
                        {!! Form::open([ 'url' => 'admin/installationDate/store','class'=>'form', 'id'=>'main_input_box']) !!}
                        <input type="hidden" name="appoint_id" value="<?php echo $_GET['id']?>">
                        <div class="form-group">
                            {!! Form::label('description', trans('installation.description') , ['class' => 'control-label required'])  !!}
                            {!! Form::text('description', null, ['class' => 'form-control']) !!}
                            <span class="help-block">{{ $errors->first('description', ':message') }}</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('installation_date', trans('installation.installation_date') , ['class' => 'control-label required']) !!}
                            {!! Form::text('installation_date', null, ['class' => 'form-control']) !!}
                            <span class="help-block">{{ $errors->first('installation_date', ':message') }}</span>
                        </div>
                        <button type="submit" class="btn btn-primary add_button">
                            Create
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Scripts --}}
@section('scripts')
    <script>
        $(document).ready(function () {
           
            var dateFormat = '{{ config('settings.date_format') }}';
            flatpickr("#installation_date", {
                minDate: '{{  now() }}',
                dateFormat: dateFormat,
            });

            $(".add_button").click(function(e){
                $.ajax({
                    type: "POST",
                    url: "admin/installationDate/create",
                    data: $("form#main_input_box").serialize(),
                    success: function (id) {
                    
                    }
                });     
            });
        });
    </script>
@stop
