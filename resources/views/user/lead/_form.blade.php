@if (isset($lead))
{!! Form::model($lead, ['url' => $type . '/' . $lead->id, 'method' => 'put', 'files'=> true]) !!}
@else
{!! Form::open(['url' => $type, 'method' => 'post', 'files'=> true]) !!}
@endif
<div class="card border-dark rounded-0 mb-4">
  <div class="card-header custom-font border-dark">
    <h4>{{ trans('lead.lead_info') }}:</h4>
  </div>
  <div class="card-body">
 <!--    <div class="row">
      <div class="col-md-12">
      </div>
      <div class="col-md-12">
        <div class="card-header custom-font mb-4">
          <h4>{{ trans('lead.personal_info') }}:</h4>
        </div>
      </div>
    </div> -->
    <div class="row">
      <div class="col-12 col-md-6">
        <div class="form-group required {{ $errors->has('title') ? 'has-error' : '' }}">
          {!! Form::label('title', trans('lead.title'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::select('title', $titles, null, ['id'=>'title', 'class' => 'form-control']) !!}
            <span class="help-block">{{ $errors->first('title', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6">
        <div class="form-group required {{ $errors->has('contact_name') ? 'has-error' : '' }}">
          {!! Form::label('contact_name', trans('lead.contact_name'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
            <span class="help-block">{{ $errors->first('contact_name', ':message') }}</span>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group required {{ $errors->has('country_id') ? 'has-error' : '' }}">
          {!! Form::label('country_id', trans('lead.country'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::select('country_id', $countries, null, ['id'=>'country_id', 'class' => 'form-control']) !!}
            <span class="help-block">{{ $errors->first('country_id', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group required {{ $errors->has('post_code') ? 'has-error' : '' }}">
          {!! Form::label('post_code', trans('lead.post_code'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::text('post_code', null, ['id'=>'post_code', 'class' => 'form-control']) !!}
            <span class="help-block">{{ $errors->first('post_code', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group required {{ $errors->has('city') ? 'has-error' : '' }}">
          {!! Form::label('city', trans('lead.city'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::text('city',null, ['id'=>'city', 'class' => 'form-control select2']) !!}
            <span class="help-block">{{ $errors->first('city', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group required {{ $errors->has('address') ? 'has-error' : '' }}">
          {!! Form::label('address', trans('lead.address'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
            <span class="help-block">{{ $errors->first('address', ':message') }}</span>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group required {{ $errors->has('phone') ? 'has-error' : '' }}">
          {!! Form::label('phone', trans('lead.phone'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::number('phone', null, ['class' => 'form-control','data-fv-integer' => "true"]) !!}
            <span class="help-block">{{ $errors->first('phone', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group required {{ $errors->has('mobile') ? 'has-error' : '' }}">
          {!! Form::label('mobile', trans('lead.mobile'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::number('mobile', null, ['class' => 'form-control','data-fv-integer' => 'true']) !!}
            <span class="help-block">{{ $errors->first('mobile', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group required {{ $errors->has('email') ? 'has-error' : '' }}">
          {!! Form::label('email', trans('lead.email'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
            <span class="help-block">{{ $errors->first('email', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group required {{ $errors->has('priority') ? 'has-error' : '' }}">
          {!! Form::label('priority', trans('lead.priority'), ['class' => 'control-label required']) !!}
          <div class="controls">
            {!! Form::select('priority', $priority, null, ['id'=>'priority','class' => 'form-control']) !!}
            <span class="help-block">{{ $errors->first('priority', ':message') }}</span>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Age Bracket</label>
          <div class="controls">
            <div class="row">
              <div class="col-md-3">
                <label class="custom-radio">20 - 40
                <input type="radio" value="20-40" name="age_bracket"  @if(!empty($lead))
                      {{ ( ($lead->age_bracket) == "20-40") ?  "checked" : "" }}
                    @endif>
                <span class="checkmark"></span>
                </label>
              </div>
              <div class="col-md-3">
                <label class="custom-radio">41 - 60
                <input type="radio" value="41-30" name="age_bracket"  @if(!empty($lead))
                      {{ ( ($lead->age_bracket) == "41-60") ?  "checked" : "" }}
                    @endif>
                <span class="checkmark"></span>
                </label>
              </div>
              <div class="col-md-3">
                <label class="custom-radio">61 - 70
                <input type="radio" value="61-70" name="age_bracket"  @if(!empty($lead))
                      {{ ( ($lead->age_bracket) == "61-70") ?  "checked" : "" }}
                    @endif>
                <span class="checkmark"></span>
                </label>
              </div>
              <div class="col-md-3">
                <label class="custom-radio">71 - 80
                <input type="radio" value="71-80" name="age_bracket"  @if(!empty($lead))
                      {{ ( ($lead->age_bracket) == "71-80") ?  "checked" : "" }} @else {{ "checked"}}
                    @endif>
                <span class="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="controls">
            <div class="row">
              <!-- <div class="col-md-8">
                <label>Customer Spoken to?</label>
                <div class="row">
                  <div class="col-md-6">
                    <label class="custom-checkbox">Spoke to Mr.
                    <input type="checkbox" checked="checked" value="spoketo_mr" name="customer_spoken[]">
                    <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label class="custom-checkbox">Spoke to Mrs.
                    <input type="checkbox" checked="checked" value="spoketo_mrs" name="customer_spoken[]">
                    <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div> -->
              <div class="col-md-4">
                <label>Both present?</label>
                <div class="row">
                  <div class="col-md-6">
                    <label class="custom-radio">Yes
                    <input type="radio" value="yes" name="both_present" @if(!empty($lead))
                      {{ ( ($lead->both_present) == "yes") ?  "checked" : "" }}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label class="custom-radio">No
                    <input type="radio" value="no" name="both_present" @if(!empty($lead))
                      {{ ( ($lead->both_present) == "no") ?  "checked" : "" }} @else {{ "checked"}}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="controls">
            <div class="row">
              <div class="col-md-8">
                <label>Decision maker?</label>
                <div class="row">
                  <div class="col-md-6">
                    <label class="custom-radio">Yes
                    <input type="radio" value="yes" name="own_decision" @if(!empty($lead))
                      {{ ( ($lead->own_decision) == "yes") ?  "checked" : "" }}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label class="custom-radio">No
                    <input type="radio" value="no" name="own_decision" @if(!empty($lead))
                      {{ ( ($lead->own_decision) == "no") ?  "checked" : "" }} @else {{ "checked"}}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <label>Are they homeowner?</label>
                <div class="row">
                  <div class="col-md-4">
                    <label class="custom-radio">Yes
                    <input type="radio" value="yes" name="are_they_homeowner" @if(!empty($lead))
                      {{ ( ($lead->are_they_homeowner) == "yes") ?  "checked" : "" }}
                    @endif  >
                    <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label class="custom-radio">No
                    <input type="radio" value="no" name="are_they_homeowner" @if(!empty($lead))
                      {{ ( ($lead->are_they_homeowner) == "no") ?  "checked" : "" }} @else {{ "checked"}}
                    @endif >
                    <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="controls">
            <div class="row">
              <div class="col-md-12">
                <label>Are they The Sole owner?</label>
                <div class="row">
                  <div class="col-md-4">
                    <label class="custom-radio">Yes
                    <input type="radio" value="yes" name="sole_owner" @if(!empty($lead))
                      {{ ( ($lead->sole_owner) == "yes") ?  "checked" : "" }}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label class="custom-radio">No
                    <input type="radio" value="no" name="sole_owner" @if(!empty($lead))
                      {{ ( ($lead->sole_owner) == "no") ?  "checked" : "" }} @else {{ "checked" }}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="controls">
            <div class="row">
              <div class="col-md-8">
                <label>Do they receive Pension Credits?</label>
                <div class="row">
                  <div class="col-md-6">
                    <label class="custom-radio">Yes
                    <input type="radio" value="yes" name="receive_pension" @if(!empty($lead))
                      {{ ( ($lead->receive_pension) == "yes") ?  "checked" : "" }}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <label class="custom-radio">No
                    <input type="radio" value="no" name="receive_pension" @if(!empty($lead))
                      {{ ( ($lead->receive_pension) == "no") ?  "checked" : "" }} @else {{ "checked" }}
                    @endif>
                    <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class=col-md-6>
        <div class="form-group">
          <div class="controls">
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                  <label>Approximate age of property</label>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="age_of_property" @if(!empty($lead))
                      value ="{{ $lead->age_of_property }}"
                    @endif>
                    <div class="input-group-append">
                      <span class="input-group-text">Years</span>
                    </div>
                  </div>
                  </div> 
              </div>
              <div class="col-md-12">
                <label>Exterior finish</label>
                <div class="row">
                 <div class="col-md-3">
                    <label class="custom-radio">Brick
                      <input type="radio" value="brick" name="finish" @if(!empty($lead))
                      {{ ( ($lead->finish) == "brick") ?  "checked" : "" }}
                    @endif>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                 <div class="col-md-3">
                    <label class="custom-radio">Render
                      <input type="radio" value="render" name="finish" @if(!empty($lead))
                      {{ ( ($lead->finish) == "render") ?  "checked" : "" }} @else {{ "checked" }}
                    @endif>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                   <input type="text" name="other" placeholder="Others" class="form-control" @if(!empty($lead))
                      value ="{{ $lead->other }}"
                    @endif>
                  </div>
                  </div>
              </div>
            </div>

             <div class="row mt-4">
              <div class="col-md-6">
                <label>Any Damp issue?</label>
                <div class="row">
                   <div class="col-md-6">
                      <label class="custom-radio">Yes
                        <input type="radio" value="yes" name="dump_issue"  @if(!empty($lead))
                      {{ ( ($lead->dump_issue) == "yes") ?  "checked" : "" }}
                    @endif>
                        <span class="checkmark"></span>
                      </label>
                    </div>
                    <div class="col-md-6">
                        <label class="custom-radio">No
                          <input type="radio" value="no" name="dump_issue"  @if(!empty($lead))
                      {{ ( ($lead->dump_issue) == "no") ?  "checked" : "" }} @else {{ "checked" }}
                    @endif>
                          <span class="checkmark"></span>
                        </label>
                    </div>
                  </div>  
              </div>
              <div class="col-md-6">
              <label>Any Insulation?</label>
                  <div class="row">
                    <div class="col-md-6">
                      <label class="custom-radio">Yes
                        <input type="radio" value="yes" name="insulation" @if(!empty($lead))
                      {{ ( ($lead->insulation) == "yes") ?  "checked" : "" }}
                    @endif>
                        <span class="checkmark"></span>
                      </label>
                    </div>
                    <div class="col-md-6">
                      <label class="custom-radio">No
                        <input type="radio" value="no" name="insulation" @if(!empty($lead))
                      {{ ( ($lead->insulation) == "no") ?  "checked" : "" }} @else {{ "checked" }}
                    @endif>
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>  
              </div>
            </div>

            <div class="row mt-4">
              <div class="col-md-12">
                <label>When?</label>
                <div class="input-group mb-3">
                  <input type="text" class="form-control" id="when" name="when" @if(!empty($lead))
                      value ="{{ $lead->when }}"
                    @endif>
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="controls">
            <div class="row">
              <div class="col-md-12">
                <label>Property Type</label>
                  <div class="row">
                     <div class="col-md-4">
                        <label class="custom-radio">House
                          <input type="radio" value="house" name="property_type" @if(!empty($lead))
                      {{ ( ($lead->property_type) == "house") ?  "checked" : "" }} 
                    @endif>
                          <span class="checkmark"></span>
                        </label>
                     </div>
                     <div class="col-md-6">
                       <label class="custom-radio">Bungalow
                        <input type="radio" value="bungalow" name="property_type" @if(!empty($lead))
                      {{ ( ($lead->property_type) == "bungalow") ?  "checked" : "" }}
                    @endif>
                        <span class="checkmark"></span>
                      </label>
                     </div>
                  </div>  
                  <div class="row">
                     <div class="col-md-4">
                        <label class="custom-radio">Terrace
                          <input type="radio" value="terrace" name="property_type" @if(!empty($lead))
                      {{ ( ($lead->property_type) == "terrace") ?  "checked" : "" }} 
                    @endif>
                          <span class="checkmark"></span>
                        </label>
                     </div>
                     <div class="col-md-4">
                       <label class="custom-radio">Semi Detached
                        <input type="radio" value="semi_detached" name="property_type" @if(!empty($lead))
                      {{ ( ($lead->property_type) == "semi_detached") ?  "checked" : "" }} 
                    @endif>
                        <span class="checkmark"></span>
                      </label>
                     </div>
                     <div class="col-md-4">
                       <label class="custom-radio">Detached
                        <input type="radio" value="detached" name="property_type" @if(!empty($lead))
                      {{ ( ($lead->property_type) == "detached") ?  "checked" : "" }} @else {{ "checked" }}
                    @endif>
                        <span class="checkmark"></span>
                      </label>
                     </div>
                  </div>    
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>  
</div>
  <div class="card border-dark rounded-0 mb-3">
    <div class="card-header custom-font ">
      <h4>GDPR Statement</h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="tab-data text-center">
            <p>This customer agrees to the company holding their data</p>
              <div id="user_company_holding" class="btn-group">
              <a class="btn btn-primary btn-sm active" data-toggle="company_holding" data-title="yes">Yes</a>
              <a class="btn btn-primary btn-sm noActive" data-toggle="company_holding" data-title="no">No</a>
              </div>
              <input type="hidden" name="company_holding" id="company_holding" value="yes">
          </div>
        </div>
        <div class="col-md-6">
          <div class="tab-data text-center">
            <p>This customer agrees to the company contacting them in the future</p>
              <div id="user_company_contacting" class="btn-group">
              <a class="btn btn-primary btn-sm active" data-toggle="company_contacting" data-title="yes">Yes</a>
              <a class="btn btn-primary btn-sm noActive" data-toggle="company_contacting" data-title="no">No</a>
              </div>
              <input type="hidden" name="company_contacting" id="company_contacting" value="yes">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="task-tab-section col-md-12">
      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          <div class="row">
            <div class="col-md-6">
              <div class="tab-data">
                <div class="tab-header">
                  Add Notes
                </div>
                <textarea name="notesckeditor"> {{ !empty($notesData->notes) ? $notesData->notes : "" }}</textarea>
                <span class="help-block">{{ $errors->first('notesckeditor', ':message') }}</span>
              </div>
            </div>
            <div class="col-md-6">
              <div class="tab-data">
                <div class="tab-header">
                  New Customer
                </div>
                <div class="tab-body scrollable-div">
                  <!-- @foreach( $noteHistory as $note)
                  <div class="note-history">
                    <p>
                      {{$note->notes}}
                    </p>
                    <small>{{ date('j F Y , h:i A',strtotime($note->created_at)) }} 
                    Test User</small>
                    <hr/>
                  </div>
                  @endforeach   -->
                  @foreach( $customers as $customer)
                  <div class="note-history">
                    <p>
                      Name : {{$customer->titles}} {{$customer->name}} ||  Phone : {{$customer->mobile}}
                    </p> 
                    <hr/>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="row">
            <div class="col-md-12">
              <div class="tab-data">
                <div class="tab-header">
                  Phone Controls
                </div>
                <div class="d-flex justify-content-space-between p-4">
                  <button type="button" class="btn form-control basic-btn m-1">
                  <i class="fa fa-pause mr-1"></i>Hold</button>    
                  <button type="button" class="btn form-control basic-btn m-1">
                  <i class="fa fa-phone mr-1"></i>Hangup</button>    
                  <button type="button" class="btn form-control basic-btn m-1">
                  <i class="fa fa-exchange mr-1"></i>Transfer & Hangup</button>
                  <button type="button" class="btn form-control basic-btn m-1">
                  <i class="fa fa-pause-circle-o mr-1"></i>Pause After Call</button>  
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="tab-data">
                <div class="tab-header">
                  Call Actions
                </div>
                <div class="tab-body wid-70">
                  <div class="row">
                    <div class="col-md-6">
                      <button type="button" class="btn form-control basic-btn">Send for scheduling</button>    
                    </div>
                    <div class="col-md-6">
                      <input type="button" name="schedule_callback" id="schedule_callback" placeholder="Schedule Callback" class="btn form-control basic-btn">
                    </div>
                  </div>
                  <div class="row mt-4">
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-6">
                          <button type="button" id="not_interested" name="not_interested" value="1" class="btn form-control basic-btn">Not Interested</button>
                        </div>
                        <div class="col-md-6">
                          <button type="button" id="do_not_call" name="do_not_call" class="btn form-control basic-btn" value="1">Do Not Call</button>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-6">
                          <button type="button" id="wrong_number" name="wrong_number" value="1" class="btn form-control basic-btn">Wrong Number</button>
                        </div>
                        <div class="col-md-6">
                          <button type="button" id="outside_criteria" name="outside_criteria" value="1" class="btn form-control basic-btn">Outside Criteria</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" id="not_interested_responseCall" name="not_interested_responseCall" value="">
                  <input type="hidden" id="do_not_call_responseCall" name="do_not_call_responseCall" value="">
                  <input type="hidden" id="wrong_number_responseCall" name="wrong_number_responseCall" value="">
                  <input type="hidden" id="outside_criteria_responseCall" name="outside_criteria_responseCall" value="">
                </div>
              </div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-md-12">
              <div class="tab-data">
                <div class="tab-body wid-70">
                  <div class="row">
                    <div class="col-md-6">
                      <a href="{{ route($type.'.index') }}" class="btn form-control basic-btn"> {{trans('table.cancel')}}</a>   
                    </div>
                
                    <div class="col-md-6">
                      <!-- <div class="save_button"> -->
                       <!--  <span>Confirm</span> -->
                        <!-- <input type="button" name="confirm" id="confirm" placeholder="Confirm" class="btn basic-confirm form-control"> -->
                        <button type="button" name="confirm" id="confirm" placeholder="Confirm" class="btn basic-confirm form-control">{{trans('lead.confirm')}}</button>
                        <button type="submit" style="display: none" class="btn basic-confirm form-control ok_confirm"> {{trans('table.ok')}}</button>
                     <!--  </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="reminderModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h4 id="modalTitle" class="modal-title">
                            Update Reminder
                          </h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                      </div>
                      <div id="modalBody" class="modal-body">
                        <label>Reminder Date/Time</label>
                        <input type="button" name="reminder" id="reminder" class="form-control">
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-primary" data-dismiss="modal" id="cancel_reminder"><i class="fa fa-fw fa-times"></i>{{trans('lead.cancel')}}</button>
                          <button type="button" class="btn btn-primary" data-dismiss="modal" id="save_reminder"><i class="fa fa-fw fa-check"></i>{{trans('lead.save')}}</button>
                      </div>
                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
<!-- ./ form actions -->
{!! Form::close() !!}
@section('scripts')
<script>
  $(document).ready(function(){
      $("#country_id").select2({
          theme:"bootstrap",
          placeholder:"{{ trans('company.select_country') }}"
      });
      $("#state_id").select2({
          theme:"bootstrap",
          placeholder:"{{ trans('company.select_state') }}"
      });
      $("#city_id").select2({
          theme:"bootstrap",
          placeholder:"{{ trans('company.select_city') }}"
      });
      $("#function").select2({
          theme: "bootstrap",
          placeholder: "{{ trans('lead.function') }}"
      });
      $("#title").select2({
          theme:"bootstrap",
          placeholder:"{{ trans('lead.title') }}"
      });
      $("#priority").select2({
          theme:"bootstrap",
          placeholder:"{{ trans('lead.priority') }}"
      });
  });

  $("#confirm").click(function(){
      $('#reminderModal').modal();
  });

  $('#save_reminder').click(function(){
    $('.ok_confirm').show();
    $('#confirm').hide();
  })

  $("#reminder").flatpickr({
    minDate: "today",
    enableTime: true,
    dateFormat: "Y-m-d H:i",
    inline:true,
    defaultDate: ["today"],
  });

  $("#state_id").find("option:contains({{trans('company.select_state')}})").attr({
      selected: true,
      value: ""
  });
  $("#city_id").find("option:contains('{{trans('company.select_city')}}')").attr({
      selected: true,
      value: ""
  });
  $('#country_id').change(function () {
      getstates($(this).val());
  });
  //button action
  $("#not_interested").click(function(){
        var responseCall = $("#not_interested").val();
        $("#not_interested_responseCall").val(responseCall);
        document.getElementById("do_not_call").style.backgroundColor = "#696969";
        document.getElementById("wrong_number").style.backgroundColor = "#696969";
        document.getElementById("outside_criteria").style.backgroundColor = "#696969";
        document.getElementById("schedule_callback").style.backgroundColor = "#696969";
        document.getElementById("not_interested").style.backgroundColor = "#f34f4e";
        $("#do_not_call_responseCall").val('');
        $("#wrong_number_responseCall").val('');
        $("#outside_criteria_responseCall").val('');
        $("#schedule_callback_responseCall").val('');
        $("#schedule_callback").val('');
  });
  $("#do_not_call").click(function(){
        var responseCall = $("#do_not_call").val();
        $("#do_not_call_responseCall").val(responseCall);
        document.getElementById("not_interested").style.backgroundColor = "#696969";
        document.getElementById("wrong_number").style.backgroundColor = "#696969";
        document.getElementById("outside_criteria").style.backgroundColor = "#696969";
        document.getElementById("schedule_callback").style.backgroundColor = "#696969";
        document.getElementById("do_not_call").style.backgroundColor = "#f34f4e";
        $("#not_interested_responseCall").val('');
        $("#wrong_number_responseCall").val('');
        $("#outside_criteria_responseCall").val('');
        $("#schedule_callback_responseCall").val('');
        $("#schedule_callback").val('');
  });
  $("#wrong_number").click(function(){
        var responseCall = $("#wrong_number").val();
        $("#wrong_number_responseCall").val(responseCall);
        document.getElementById("not_interested").style.backgroundColor = "#696969";
        document.getElementById("do_not_call").style.backgroundColor = "#696969";
        document.getElementById("outside_criteria").style.backgroundColor = "#696969";
        document.getElementById("schedule_callback").style.backgroundColor = "#696969";
        document.getElementById("wrong_number").style.backgroundColor = "#f34f4e";
        $("#do_not_call_responseCall").val('');
        $("#not_interested_responseCall").val('');
        $("#outside_criteria_responseCall").val('');
        $("#schedule_callback_responseCall").val('');
        $("#schedule_callback").val('');
  });
  $("#outside_criteria").click(function(){
        var responseCall = $("#outside_criteria").val();
        $("#outside_criteria_responseCall").val(responseCall);
        document.getElementById("do_not_call").style.backgroundColor = "#696969";
        document.getElementById("wrong_number").style.backgroundColor = "#696969";
        document.getElementById("not_interested").style.backgroundColor = "#696969";
        document.getElementById("schedule_callback").style.backgroundColor = "#696969";
        document.getElementById("outside_criteria").style.backgroundColor = "#f34f4e";
        $("#do_not_call_responseCall").val('');
        $("#wrong_number_responseCall").val('');
        $("#not_interested_responseCall").val('');
        $("#schedule_callback_responseCall").val('');
        $("#schedule_callback").val('');
  });
  $("#schedule_callback").mouseup(function(e){
        document.getElementById("do_not_call").style.backgroundColor = "#696969";
        document.getElementById("wrong_number").style.backgroundColor = "#696969";
        document.getElementById("not_interested").style.backgroundColor = "#696969";
        document.getElementById("outside_criteria").style.backgroundColor = "#696969";
        document.getElementById("schedule_callback").style.backgroundColor = "#f34f4e";
        $("#do_not_call_responseCall").val('');
        $("#wrong_number_responseCall").val('');
        $("#not_interested_responseCall").val('');
        $("#outside_criteria_responseCall").val('');
  });

  $("#schedule_callback").flatpickr({
      minDate: "today",
      enableTime: true,
      dateFormat: "Y-m-d H:i:ss",
  });

  @if(old('country_id'))
  getstates({{old('country_id')}});
  
  @endif
  function getstates(country) {
      $.ajax({
          type: "GET",
          url: '{{ url('lead/ajax_state_list')}}',
          data: {'id': country, _token: '{{ csrf_token() }}'},
          success: function (data) {
              $('#state_id').empty();
              $('#city_id').empty();
              $('#state_id').select2({
                  theme: "bootstrap",
                  placeholder: "{{trans('company.select_state')}}"
              }).trigger('change');
              @if(old('state_id'))
              getcities({{old('state_id')}});
              @endif
              $('#city_id').select2({
                  theme: "bootstrap",
                  placeholder: "{{trans('company.select_city')}}"
              }).trigger('change');
              $.each(data, function (val, text) {
                  $('#state_id').append($('<option></option>').val(val).html(text).attr('selected', val == "{{old('state_id')}}" ? true : false));
              });
          }
      });
  }
  
  $('#state_id').change(function () {
      getcities($(this).val());
  });
  function getcities(cities) {
      $.ajax({
          type: "GET",
          url: '{{ url('lead/ajax_city_list')}}',
          data: {'id': cities, _token: '{{ csrf_token() }}'},
          success: function (data) {
              $('#city_id').empty();
              $('#city_id').select2({
                  theme: "bootstrap",
                  placeholder: "{{trans('company.select_city')}}"
              }).trigger('change');
              $.each(data, function (val, text) {
                  $('#city_id').append($('<option></option>').val(val).html(text).attr('selected', val == "{{old('city_id')}}" ? true : false));
              });
          }
      });
  }
</script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'notesckeditor' );
</script>
<script>
  $(document).ready(function () {

      flatpickr("#when", {
          minDate: "today",
          dateFormat: "Y-m-d"
      });

      $("#user_company_holding a, #user_company_contacting a").on('click', function(){
        var selected = $(this).data('title');
        var toggle = $(this).data('toggle');
        $('#'+toggle).prop('value', selected);
        $('a[data-toggle="'+toggle+'"]').not('[data-title="'+selected+'"]').removeClass('active').addClass('noActive');
        $('a[data-toggle="'+toggle+'"][data-title="'+selected+'"]').removeClass('noActive').addClass('active');
        if(selected == "no"){
          alert('Please allow GDPR for saving information ');
          $('#confirm').attr("disabled", "disabled");
        }else{
          $('#confirm').removeAttr("disabled"); 
        }
      })
  });
</script>
@endsection