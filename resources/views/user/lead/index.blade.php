@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop
{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        @if($user->hasAccess(['leads.write']) || $orgRole=='admin')
            <div class="pull-right">
                <a href="{{ $type.'/create' }}" class="btn btn-primary m-b-10">
                    <i class="fa fa-plus-circle"></i> {{ trans('lead.new') }}</a>
            </div>
        @endif
    </div>
    <div class="card">
      <div class="card-header bg-white">
        <h4 class="float-left">
          <i class="material-icons">thumb_up</i>
          {{ $title }}
        </h4>
        <span class="pull-right">
          <i class="fa fa-fw fa-chevron-up clickable"></i>
          <i class="fa fa-fw fa-times removecard clickable"></i>
        </span>
      </div>
      <div class="card-body">

        <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Customer#</th>
                <th>Customer Name</th>
                <th>Last Notes</th>
                <th>Reminder</th>
                <th>Set</th>
                <th>Cancel</th>
                <th>Sold</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
          @foreach($leadsData as $key => $lead)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{date('d-m-Y H:i:s',strtotime($lead->created_at))}}</td>
                <td>{{ $lead->customer_reference_no }}</td>
                <td>{{$lead->contact_name}}</td>
                <td>{{empty($lead->getNotes->notes)? "" : $lead->getNotes->notes}}</td>
                <td>{{ empty($lead->getAppointment->assign_date) ? "Not Available" : date('d-m-Y @ H:i',strtotime($lead->getAppointment->assign_date))}}</td>
                <td><a href="{{ url('lead/' . $lead->id . '/edit' ) }}" title="{{ trans('table.edit') }}"><i class="fa fa-fw fa-pencil text-warning"></i></a></td>
                <td><a href="{{ url('lead/' . $lead->id . '/delete' ) }}" title="{{ trans('table.delete') }}"><i class="fa fa-fw fa-times text-danger"></i></a></td>
                <td>
                @if( $lead->schedule_callback == 1 && !empty($lead->getAppointment->sale_completed) == 1)
                    <a href="{{ url('lead/') }}" title="{{ trans('table.sold') }}"><i class="fa fa-fw fa-check text-success"></i></a>
                @elseif( ($lead->schedule_callback == 1) && !empty($lead->getAppointment->sale_completed == "0") )
                    Not Completed
                @else
                    Close the job
                @endif
                </td>
                <td><a href="{{ url('lead/' . $lead->id . '/show' ) }}" title="{{ trans('table.details') }}" ><i class="fa fa-fw fa-eye text-primary"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
      </div>
    </div>
@stop

{{-- Scripts --}}
@section('scripts')
    <script>

      $(document).ready(function() {
        $('#example').DataTable();
      } );
        var chart1 = c3.generate({
            bindto: '#leads_priority',
            data: {
                columns: [
                        @foreach($priorities as $item)
                    ['{{$item['value']}}', {{$item['leads']}}],
                    @endforeach
                ],
                type : 'donut',
                colors: {
                    @foreach($priorities as $item)
                    '{{$item['value']}}': '{{$item['color']}}',
                    @endforeach
                }
            }
        });
        setTimeout(function () {
            chart1.resize()
        }, 500);


        //products by month
        var productsData = [
            ['leads'],
                @foreach($graphics as $item)
            [{{$item['leads']}}],
            @endforeach
        ];
        var chart2 = c3.generate({
            bindto: '#leads_by_month',
            data: {
                rows: productsData,
                type: 'bar'
            },
            color: {
                pattern: ['#3295ff']
            },
            axis: {
                x: {
                    tick: {
                        format: function (d) {
                            return formatMonth(d);
                        }
                    }
                }
            },
            legend: {
                show: true,
                position: 'bottom'
            },
            padding: {
                top: 10
            }
        });

        function formatMonth(d) {
            @foreach($graphics as $id => $item)
            if ('{{$id}}' == d) {
                return '{{$item['month']}}' + ' ' + '{{$item['year']}}'
            }
            @endforeach
        }

        $(".sidebar-toggle").on("click",function () {
            setTimeout(function () {
                chart1.resize();
                chart2.resize();
            },200)
        });


    </script>
    @if(isset($type))
        <script type="text/javascript">
            var oTable;
            $(document).ready(function () {
                oTable = $('#data').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    columns:[
                        {"data":"created_at"},
                        {"data":"company_name"},
                        {"data":"contact_name"},
                        {"data":"product_name"},
                        {"data":"email"},
                        {"data":"phone"},
                        {"data":"priority"},
                        {"data":"actions"}
                    ],
                    "ajax": "{{ url($type) }}" + ((typeof $('#data').attr('data-id') != "undefined") ? "/" + $('#id').val() + "/" + $('#data').attr('data-id') : "/data")
                });
                $('div.dataTables_length select').select2({
                    theme:"bootstrap"
                });
            });
        </script>
    @endif

@stop
