@extends('layouts.user')

{{-- Web site Title --}}
@section('title')

@stop

{{-- Content --}}
@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-white">
                    <h4 class="float-left">
                        <i class="livicon" data-name="inbox" data-size="18" data-color="white" data-hc="white"
                           data-l="true"></i>
                        {{ trans('appointment.my_appointment_list') }}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table id="data" class="table table-striped table-bordered ">
                        <thead>
                        <tr>
                            <th>{{ trans('appointment.description') }}</th>
                            <th>{{ trans('appointment.deadline') }}</th>
                            <th>{{ trans('appointment.lead') }}</th>
                            <th>{{ trans('appointment.sale_completed') }}</th>
                            <th>{{ trans('appointment.job_quote') }} </th>
                            <th>{{ trans('appointment.not_sold') }}</th>
                        </tr>
                        @foreach($appointments as $appointment)
                        <tr>
                            <input type="hidden" class="idval" value="{{$appointment->id}}">
                            <td>{{ $appointment->title }}</td>
                            <td>{{ date('Y-m-d',strtotime($appointment->assign_date)) }}</td>
                            <td>{{ empty($appointment->getLead->product_name) ? "" : $appointment->getLead->product_name }}</td>
                            <td>
                            <select name="sale_completed" data-id="{{$appointment->id}}" class="sale_completed">
                                <option value="0" {{($appointment->sale_completed == 0) ? "selected" : ""}}>NO</option>
                                <option value="1" {{($appointment->sale_completed == 1) ? "selected" : ""}}>Yes</option>
                            </select>
                            <?php $setorderData = 0 ?>
                            @foreach($orders as $order)
                                @if($order->lead_id == $appointment->lead_id)
                                    <?php $setorderData = 1 ?>
                                @endif
                            @endforeach()
                            @if( $appointment->sale_completed == 1 && empty($setorderData))  
                                <a href="{{ url('/invoice')}}?id={{$appointment->lead_id}}" class="order_div" target="_blank">Create Order</a>
                            @elseif( $appointment->sale_completed == 1 && !empty($setorderData) )
                                Order Created.
                            @endif

                            </td>
                            <td><textarea name="job_quote" class="job_quote" data-id="{{$appointment->id}}">{{ $appointment->job_quote }}</textarea></td>
                            <td>
                            <select name="not_sold" class="not_sold" data-id="{{$appointment->id}}">
                                <option value="0" {{($appointment->not_sold == 0) ? "selected" : ""}}>NO</option>
                                <option value="1" {{($appointment->not_sold == 1) ? "selected" : ""}}>Yes</option>
                            </select>
                            </td>
                        </tr>
                        @endforeach
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Scripts --}}
@section('scripts')
    <script>
        $(document).ready(function () {
            $(".sale_completed").select2({
                theme: "bootstrap",
            });
            $(".not_sold").select2({
                theme: "bootstrap",
            });

            $(".sale_completed").change(function(e){
                updateSoldComplete(this)
            })

            $(".not_sold").change(function(e){
                updateNotSold(this)
            })

            $(".job_quote").change(function(e){
                changeJobQuote(this)
            })

        });
       
        function updateSoldComplete(obj){
            var id = obj.dataset.id
            var selectVal = obj.value
            $.ajax({
                data: {  id : id , selectVal : selectVal },
                type: "GET",
                url: "{{ url('user/appointment/update') }}",
                success:function(data){
                    if(selectVal == "1"){
                        $(".order_div").show(); 
                    }else{
                        $(".order_div").hide();
                    }
                    location.reload();
                    alert("Sold Change Seccessfully.");
                },
                error:function(data){
                  console.log(data);
                }
            });
        }
        function updateNotSold(obj){
            var id = obj.dataset.id
            var selecteditem = obj.value
            $.ajax({
                data: { id : id , selecteditem : selecteditem },
                type: "GET",
                url: "{{ url('user/appointment/notSoldUpdate') }}",
                success:function(data){
                    alert("Not Sold Status Changed");
                },
                error:function(data){
                    alert(data);
                }
            });
        }
        function changeJobQuote(obj){
            var id = obj.dataset.id
            var quotes = obj.value
            $.ajax({
                data:{ id: id , quotes : quotes },
                type: "GET",
                url: "{{ url('user/appointment/updateJobQuote') }}",
                success: function(data){
                    alert("Quotes Updated ")
                },
                error:function(data){
                    alert(data);
                }
            });
        }
    </script>
@stop