@extends('layouts.frontend.user')
@section('styles')
    <link href="{{ asset('css/login_register.css') }}" rel="stylesheet" type="text/css">
@stop
@section('content')
    <section class="login-wrapper">
        <div class="row no-margin">
            <div class="col-md-6 no-padding"></div>
            <div class="m-auto col-lg-6 col-md-6 col-sm-10 col-12 no-padding">
                <div class="signin-form">
                    <div class="box-color">
                        <h4 class="text-center text-white">{{trans('auth.login')}}</h4>
                        <div class="row">
                            <div class="col-12 m-t-20">
                                <div class="logo-box">
                                    <img src="{!! url('/').'/uploads/site/spectrum-logo.jpg' !!} ">
                                    <p>{{trans('auth.welcome')}}</p>
                                </div> 
                                {!! Form::open(['url' => url('signin'), 'method' => 'post', 'name' => 'form']) !!}
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    
                                    {!! Form::email('email', null, ['class' => 'form-control', 'required'=>'required', 'placeholder'=>'E-mail', 'autofocus'=>true]) !!}
                                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    
                                    {!! Form::password('password', ['class' => 'form-control', 'required'=>'required', 'placeholder'=>'Password']) !!}
                                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>
                                                <input type="checkbox" id="remember" value="remember" name="remember">
                                                <i class="primary"></i> {{trans('auth.keep_login')}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-6 forgot-box">
                                        <label>
                                            <a href="{{url('forgot')}}" class="forgot_pw _600">{{trans('auth.forgot')}}?</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-block submit-btn">{{trans('auth.login')}}</button>
                                    </div>
                                    <!-- <div class="col-6">
                                        <a href="{{url('register')}}" class="btn btn-success btn-block">{{trans('auth.signup')}}</a>
                                    </div> -->
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop


<style type="text/css">
    body{
  background-image: url("../uploads/site/login-wrapper.jpg") !important;
  background-size: cover;
  background-repeat: no-repeat;

}
.mt-5, .my-5 {
    margin-top: 0 !important;
}
</style>