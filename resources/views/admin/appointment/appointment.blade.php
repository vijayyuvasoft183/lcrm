@extends('layouts.user')

{{-- Web site Title --}}
@section('title')

@stop

{{-- Content --}}
@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-white">
                    <h4 class="float-left">
                        <i class="livicon" data-name="inbox" data-size="18" data-color="white" data-hc="white"
                           data-l="true"></i>
                        {{ trans('appointment.my_appointment_list') }}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table id="data" class="table table-striped table-bordered ">
                        <thead>
                        <tr>
                            <th>{{ trans('appointment.description') }}</th>
                            <th>{{ trans('appointment.deadline') }}</th>
                            <th>{{ trans('appointment.lead') }}</th>
                            <th>{{ trans('appointment.user') }}</th>
                            <th>{{ trans('appointment.assignto') }} </th>
                            <th>{{ trans('appointment.status') }}</th>
                        </tr>
                        @foreach($appointments as $appointment)
                        <tr>
                            <input type="hidden" class="idval" value="{{$appointment->id}}">
                            <td>{{ $appointment->title }}</td>
                            <td>{{ date('Y-m-d',strtotime($appointment->assign_date)) }}</td>
                            <td>{{ empty($appointment->getLead->product_name) ? "" : $appointment->getLead->product_name }}</td>
                            <td>{{ $appointment->getUserName->first_name }}</td>
                            <td>
                            <select name="assign" data-id="{{$appointment->id}}" class="assign">
                                <option value="">-- Select --</option>
                                @foreach($usersData as $userData)
                                <option value="{{ $userData->id }}" {{ ($appointment->assign_id == $userData->id ) ? "selected" : ""}}>{{ $userData->first_name }}</option>
                                @endforeach
                            </select>
                            </td>
                            <td>
                            <select name="status" class="status">
                                <option value="0" {{($appointment->sale_completed == 0) ? "selected" : ""}}>Pending</option>
                                <option value="1" {{($appointment->sale_completed == 1) ? "selected" : ""}}>Done</option>
                            </select>
                            <?php $setorderData = 0 ?>
                            @foreach($orders as $order)
                                @if($order->lead_id == $appointment->lead_id)
                                    <?php $setorderData = 1 ?>
                                @endif
                            @endforeach()

                            <?php $setInstallerData = 0 ?>
                            @foreach($installers as $installer)
                                @if($installer->appointment_id == $appointment->id)
                                    <?php $setInstallerData = 1 ?>
                                @endif
                            @endforeach()

                            @if( $appointment->sale_completed == 1 && empty($setorderData))  
                               Order not created yet.
                            @elseif( $appointment->sale_completed == 1 && !empty($setorderData) && empty($setInstallerData))
                                <a href="{{ url('/admin/installationDate/create')}}?id={{$appointment->id}}" class="order_div" target="_blank">Create Installation Date </a>
                            @elseif( !empty($setInstallerData))
                                Installation Date Created.
                            @endif
                            </td>
                        </tr>
                        @endforeach
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- Scripts --}}
@section('scripts')
    <script src="{{ asset('js/appoint.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".assign").select2({
                theme: "bootstrap",
            });
            $(".status").select2({
                theme: "bootstrap",
            });
            $(".assign").change(function(e){
                updateData(this)
            });
        });
     
        function updateData(obj){
            var id = obj.dataset.id
            var selectVal = obj.value
            $.ajax({
                data: {  id : id , selectVal : selectVal },
                type: "GET",
                url: "{{ url('admin/appointment/update') }}",
                success:function(data){
                  alert("Appointment Assign Seccessfully.");
                },
                error:function(data){
                  alert(data);
                }
            });
        }
    </script>
@stop