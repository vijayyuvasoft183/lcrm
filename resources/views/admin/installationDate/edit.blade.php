@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')
    <div class="page-header clearfix">
        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header bg-white">
            <h4 class="float-left">
                <i class="material-icons">flag</i>
                {{ $title }}
            </h4>
            <span class="pull-right">
                <i class="fa fa-fw fa-chevron-up clickable"></i>
                <i class="fa fa-fw fa-times removecard clickable"></i>
            </span>
        </div>
        <div class="card-body">
           <div class="table-responsive">
               <table id="data" class="table table-striped table-bordered">
                   <thead>
                   <tr>
                       <th>#</th>
                       <th>{{ trans('installation.appointment') }}</th>
                       <th>{{ trans('installation.installationDate') }}</th>
                       <th>{{ trans('installation.action') }}</th>
                   </tr>
                   </thead>
                   <tbody>
                   	@if(!empty($installers))
                   		@foreach($installers as $key => $installer)
                   		<tr>
                   			<td><?php $key ?></td>
                   			<td>{{ empty($installer->appointment_id) ? "" : $installer->appointment_id }}</td>
                   			<td>{{ empty($installer->installation_date) ? "" : $installer->installation_date }}</td>
                   			<td>
                   				Action 	
				            </td>
				        </tr>
                   		@endforeach
                   	@endif
                   </tbody>
               </table>
           </div>
        </div>
    </div>
@stop

{{-- Scripts --}}
@section('scripts')
    <!-- Scripts -->
    @if(isset($type))
       
    @endif
@stop
