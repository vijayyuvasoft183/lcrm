<?php

return [

    "appointments" => "Appointment",
    "description" => 'Appointment Description',
    "lead" => "Lead",
    "deadline" => "Deadline",
    "my_appointment_list" => "My Appointment List",
    "not_sold" => "Not Sold",
    "job_quote" => "Job Quote",
    "sale_completed" => "Sale Completed",
    "save" => "Assign Save",
    "user" => "phone Agent",
    "assignto" => "Assign To",
    "status" => "Status"
];