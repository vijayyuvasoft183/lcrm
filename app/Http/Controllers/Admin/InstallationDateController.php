<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\InstallationDate;
use App\Http\Controllers\Controller;
use App\Http\Requests\InstallationDateRequest;
use Redirect;

class InstallationDateController extends Controller
{
    function index(){
    	$title = trans('installation.installer');
    	$installers = InstallationDate::all();
    	//print_r($installers);die;
    	return view('admin.installationDate.index',compact('title','installers'));
    }
    function create(){
    	$title = trans('installation.create');
    	return view('admin.installationDate.create',compact('title'));
    }
    function store(InstallationDateRequest $request)
    {
    	$installData = new InstallationDate;
    	$installData->appointment_id = $request->appoint_id;
    	$installData->installation_date = date('Y-m-d h:i:s',strtotime($request->installationDate));
    	$installData->description = $request->description;
    	$installData->save();
    	return Redirect('/admin/installationDate');
    }
    function edit(){

    }
    function update(){

    }
    function delete(){

    }
}
