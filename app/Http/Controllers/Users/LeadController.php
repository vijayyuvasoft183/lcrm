<?php

namespace App\Http\Controllers\Users;

use App\Helpers\ExcelfileValidator;
use App\Http\Controllers\Controller;
use App\Http\Requests\LeadImportRequest;
use App\Http\Requests\LeadRequest;
use App\Repositories\CityRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\ExcelRepository;
use App\Repositories\LeadRepository;
use App\Repositories\OptionRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\SalesTeamRepository;
use App\Repositories\StateRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use DataTables;
use App\Models\NoteHistory;
use App\Models\Appointment;
use App\Models\Lead;
use Validator;
use Input;
use Redirect;
use App\Models\Customer;

class LeadController extends Controller
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var LeadRepository
     */
    private $leadRepository;
    /**
     * @var SalesTeamRepository
     */
    private $salesTeamRepository;
    /**
     * @var OptionRepository
     */
    private $optionRepository;

    private $countryRepository;

    private $stateRepository;

    private $cityRepository;

    private $organizationRepository;

    private $excelRepository;

    protected $user;

    public function __construct(
        CompanyRepository $companyRepository,
        UserRepository $userRepository,
        LeadRepository $leadRepository,
        SalesTeamRepository $salesTeamRepository,
        OptionRepository $optionRepository,
        CountryRepository $countryRepository,
        StateRepository $stateRepository,
        CityRepository $cityRepository,
        OrganizationRepository $organizationRepository,
        ExcelRepository $excelRepository
    ) {

        parent::__construct();

        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
        $this->leadRepository = $leadRepository;
        $this->salesTeamRepository = $salesTeamRepository;
        $this->countryRepository = $countryRepository;
        $this->stateRepository = $stateRepository;
        $this->cityRepository = $cityRepository;
        $this->organizationRepository = $organizationRepository;
        $this->excelRepository = $excelRepository;

        view()->share('type', 'lead');
        $this->optionRepository = $optionRepository;
    }

    public function index()
    {
        $this->user = $this->getUser();
        /*if ((!$this->user->hasAccess(['leads.read'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        $title = trans('lead.leads');

        $priorities = $this->optionRepository->findByField('category','priority')
            ->map(function ($title) {
                return [
                    'title' => $title->title,
                    'value'   => $title->value,
                ];
            })->toArray();
        $colors = ['#3295ff','#2daf57','#fc4141','#fcb410','#17a2b8','#3295ff','#2daf57','#fc4141','#fcb410','#17a2b8'];
        foreach ($priorities as $key=>$priority){
            $priorities[$key]['color'] = isset($colors[$key])?$colors[$key]:"";
            $priorities[$key]['leads'] = $this->leadRepository->getAll()->where('priority', $priority['value'])->count();
        }

        $graphics = [];

        for ($i = 11; $i >= 0; --$i) {
            $monthno = now()->subMonth($i)->format('m');
            $month = now()->subMonth($i)->format('M');
            $year = now()->subMonth($i)->format('Y');
            $graphics[] = [
                'month' => $month,
                'year' => $year,
                'leads' => $this->leadRepository->getMonthYear($monthno, $year)->count(),
            ];
        }
        $leads = $this->leadRepository->getAll()->count();
        $leadProduct = $this->leadRepository->getAll()->where('function','Product')->count();
        $leadDesign = $this->leadRepository->getAll()->where('function','Design')->count();
        $leadSoftware = $this->leadRepository->getAll()->where('function','Software')->count();
        $leadOthers = $this->leadRepository->getAll()->where('function','Other')->count();

        $leadsData = Lead::all();

        return view('user.lead.index', compact('title','priorities','graphics','leadProduct','leadDesign','leadSoftware','leadOthers','leads','leadsData'));
    }

    public function create()
    {
        $this->generateParams();
        /*if ((!$this->user->hasAccess(['leads.write'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        $title = trans('lead.new');
        $calls = 0;
        $noteHistory = NoteHistory::where('user_id',$this->getUser()->id)->get();
        $customers = Customer::all();

        return view('user.lead.create', compact('title', 'calls','noteHistory','customers'));
    }

    public function store(LeadRequest $request)
    {
        /*$this->generateParams();
        if ((!$this->user->hasAccess(['leads.write'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        /*$rules = [           
            'company_name' => 'required',
            'email' => 'required|email',
            'assign_date' => 'required',
            'contact_name' => 'required',
            'country_id' => 'required',
           
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::to('lead/create')
                ->withErrors($validator);
        }*/
        $user = $this->userRepository->getUser();
        $organization = $this->userRepository->getOrganization();
        $request->merge(['user_id'=>$user->id,'organization_id'=>$organization->id]);
        //$this->leadRepository->create($request->all());

        $leads = new Lead;
        $leads->title = $request->title;
        $leads->customer_reference_no = rand(100,100000);
        $leads->contact_name = $request->contact_name;
        $leads->country_id = $request->country_id;
        $leads->post_code = $request->post_code;
        $leads->city = $request->city;
        $leads->address = $request->address;
        $leads->phone = $request->phone;
        $leads->mobile = $request->mobile;
        $leads->email = $request->email;
        $leads->organization_id = $organization->id;
        $leads->priority = $request->priority;
        $leads->user_id = $user->id;
        $leads->age_bracket = $request->age_bracket;
        $leads->customer_spoken = serialize($request->customer_spoken);
        $leads->both_present = $request->both_present;
        $leads->own_decision = $request->own_decision;
        $leads->are_they_homeowner = $request->are_they_homeowner;
        $leads->sole_owner = $request->sole_owner;
        $leads->receive_pension = $request->receive_pension;
        $leads->age_of_property = $request->age_of_property;
        $leads->finish = $request->finish;
        $leads->other = $request->other;
        $leads->dump_issue = $request->dump_issue;
        $leads->insulation = $request->insulation;
        $leads->when = empty($request->when) ? "" : date('Y-m-d',strtotime($request->when));
        $leads->property_type = $request->property_type;
        $leads->company_holding = $request->company_holding;
        $leads->company_contacting = $request->company_contacting;
        $leads->schedule_callback = empty($request->reminder) ? 0 : 1;
        $leads->not_interested = empty($request->not_interested_responseCall) ? 0 : 1 ;
        $leads->do_not_call = empty($request->do_not_call_responseCall) ? 0 : 1;
        $leads->wrong_number = empty($request->wrong_number_responseCall) ? 0 : 1;
        $leads->outside_criteria = empty($request->outside_criteria_responseCall) ? 0 : 1;
        $leads->save();

        //insert appointment
        if(!empty($request->reminder)){
            $appointment = new Appointment;
            $appointment->user_id = $user->id;
            $appointment->lead_id = $leads->id;
            $appointment->title = $request->contact_name;
            $appointment->assign_date   = $request->reminder;
            $appointment->save();
        }

        //Save notes
        if(!empty($request->notesckeditor)){
            $notes = new NoteHistory;
            $notes->user_id = $user->id;
            $notes->lead_id = $leads->id;
            $notes->notes = $request->notesckeditor;
            $notes->save();
        }
        return redirect('lead');
    }

    public function edit($lead)
    {
        $this->generateParams();
        /*if ((!$this->user->hasAccess(['leads.write'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        $lead = $this->leadRepository->find($lead);
        $title = trans('lead.edit');

        $calls = $lead->calls()->count();
        $states = $this->stateRepository->orderBy('name', 'asc')->findByField('country_id', $lead->country_id)->pluck('name', 'id');
        $cities = $this->cityRepository->orderBy('name', 'asc')->findByField('state_id', $lead->state_id)->pluck('name', 'id');
        $noteHistory = NoteHistory::where('user_id',$this->getUser()->id)->get();
        $notesData = NoteHistory::where('user_id',$this->getUser()->id)->where('lead_id',$lead->id)->first();
        $customers = Customer::all();

        return view('user.lead.edit', compact('lead', 'title', 'calls', 'states', 'cities', 'noteHistory','notesData','customers'));
    }

    public function update(LeadRequest $request,$lead)
    {
        //$this->generateParams();
        /*if ((!$this->user->hasAccess(['leads.write'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        //$lead = $this->leadRepository->find($lead);
        //$lead->update($request->all());
        $user = $this->userRepository->getUser();

        $leads = Lead::find($lead);
        $leads->title = $request->title;
        $leads->contact_name = $request->contact_name;
        $leads->country_id = $request->country_id;
        $leads->post_code = $request->post_code;
        $leads->city = $request->city;
        $leads->address = $request->address;
        $leads->phone = $request->phone;
        $leads->mobile = $request->mobile;
        $leads->email = $request->email;
        $leads->priority = $request->priority;
        $leads->age_bracket = $request->age_bracket;
        $leads->customer_spoken = serialize($request->customer_spoken);
        $leads->both_present = $request->both_present;
        $leads->own_decision = $request->own_decision;
        $leads->are_they_homeowner = $request->are_they_homeowner;
        $leads->sole_owner = $request->sole_owner;
        $leads->receive_pension = $request->receive_pension;
        $leads->age_of_property = $request->age_of_property;
        $leads->finish = $request->finish;
        $leads->other = $request->other;
        $leads->dump_issue = $request->dump_issue;
        $leads->insulation = $request->insulation;
        $leads->when = $request->when;
        $leads->property_type = $request->property_type;
        $leads->company_holding = $request->company_holding;
        $leads->company_contacting = $request->company_contacting;
        $leads->schedule_callback = empty($request->reminder) ? 0 : 1;
        $leads->not_interested = empty($request->not_interested_responseCall) ? 0 : 1 ;
        $leads->do_not_call = empty($request->do_not_call_responseCall) ? 0 : 1;
        $leads->wrong_number = empty($request->wrong_number_responseCall) ? 0 : 1;
        $leads->outside_criteria = empty($request->outside_criteria_responseCall) ? 0 : 1;
        $leads->save();

        //update appointment
        $appointment = Appointment::where('lead_id',$lead)->first();
        if(!empty($appointment) && empty($request->reminder)){
            $appointment->delete();
        }
        if(!empty($appointment) && !empty($request->reminder)){
            $appointment->title = empty($request->contact_name) ? "" : $request->contact_name;
            $appointment->assign_date   =  $request->reminder ;
            $appointment->save();
        }
        if(empty($appointment) && !empty($request->reminder)){
            $appointment = new Appointment;
            $appointment->user_id = $user->id;
            $appointment->lead_id = $leads->id;
            $appointment->title = $request->contact_name;
            $appointment->assign_date   = $request->reminder;
            $appointment->save();
        }

        //update notes
        $notes = NoteHistory::where('lead_id',$lead)->first();
        if(!empty($notes)){
            $notes->notes = $request->notesckeditor;
            $notes->save();
        }elseif(!empty($request->notesckeditor)){
            $notes = new NoteHistory;
            $notes->user_id = $user->id;
            $notes->lead_id = $leads->id;
            $notes->notes = $request->notesckeditor;
            $notes->save();
        }

        return redirect('lead');
    }

    public function show($lead)
    {
        $this->user = $this->getUser();
        /*if ((!$this->user->hasAccess(['leads.read'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        $lead = $this->leadRepository->find($lead);
        $title = trans('lead.show');
        $action = trans('action.show');
        $appointment = Appointment::where('lead_id',$lead->id)->first();
        return view('user.lead.show', compact('title', 'lead', 'action','appointment'));
    }

    public function delete($lead)
    {
        $this->user = $this->getUser();
        /*if ((!$this->user->hasAccess(['leads.delete'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        $lead = $this->leadRepository->find($lead);
        $title = trans('lead.delete');
        $appointment = Appointment::where('lead_id',$lead->id)->first();
        return view('user.lead.delete', compact('title', 'lead','appointment'));
    }

    public function destroy($lead)
    {
        $this->user = $this->getUser();
       /* if ((!$this->user->hasAccess(['leads.delete'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }*/
        $lead = $this->leadRepository->find($lead);
        $lead->delete();

        return redirect('lead');
    }

    public function data()
    {
        $this->user = $this->getUser();
        if ((!$this->user->hasAccess(['leads.read'])) && $this->user->orgRole=='staff') {
            return redirect('dashboard');
        }
        $orgRole = $this->getUser()->orgRole;
        $dateFormat = config('settings.date_format');
        $leads = $this->leadRepository->getAll()
            ->map(function ($lead) use ($orgRole,$dateFormat){
                return [
                    'id' => $lead->id,
                    'created_at' => date($dateFormat,strtotime($lead->created_at)),
                    'company_name' => $lead->company_name,
                    'contact_name' => $lead->contact_name,
                    'product_name' => $lead->product_name,
                    'email' => $lead->email,
                    'phone' => $lead->phone,
                    'calls' => $lead->calls->count(),
                    'priority' => $lead->priority,
                    'orgRole' => $orgRole
                ];
            });

        return DataTables::of($leads)
            ->addColumn('actions', '@if(Sentinel::getUser()->hasAccess([\'leads.write\']) || $orgRole=="admin")
                                        <a href="{{ url(\'lead/\' . $id . \'/edit\' ) }}" title="{{ trans(\'table.edit\') }}">
                                            <i class="fa fa-fw fa-pencil text-warning"></i> </a>
                                    @endif
                                    @if(Sentinel::getUser()->hasAccess([\'logged_calls.read\']) || $orgRole=="admin")
                                    <a href="{{ url(\'leadcall/\'. $id .\'/\' ) }}" title="{{ trans(\'table.calls\') }}">
                                            <i class="fa fa-fw fa-phone text-primary"></i> <sup>{{ $calls }}</sup></a>
                                    @endif
                                     @if(Sentinel::getUser()->hasAccess([\'leads.read\']) || $orgRole=="admin")
                                     <a href="{{ url(\'lead/\' . $id . \'/show\' ) }}" title="{{ trans(\'table.details\') }}" >
                                            <i class="fa fa-fw fa-eye text-primary"></i> </a>
                                    @endif
                                    @if(Sentinel::getUser()->hasAccess([\'leads.delete\']) || $orgRole=="admin")
                                     <a href="{{ url(\'lead/\' . $id . \'/delete\' ) }}" title="{{ trans(\'table.delete\') }}">
                                            <i class="fa fa-fw fa-trash text-danger"></i> </a>
                                    @endif
                                    <a href="{{ url(\'lead/\' . $id . \'/delete\' ) }}" title="{{ trans(\'table.delete\') }}">
                                            <i class="fa fa-fw fa-times text-danger"></i> </a>
                                    <a href="{{ url(\'lead/\' . $id . \'/sold\' ) }}" title="{{ trans(\'table.sold\') }}">
                                            <i class="fa fa-fw fa-check text-success"></i> </a>
                                    ')
            ->removeColumn('id')
            ->removeColumn('calls')
            ->rawColumns(['actions'])
            ->make();
    }

    public function ajaxStateList(Request $request)
    {
        return $this->stateRepository->orderBy('name','asc')->findByField('country_id',$request->id)->pluck('name', 'id')->prepend(trans('lead.select_state'),'');
    }

    public function ajaxCityList(Request $request)
    {
        return $this->cityRepository->orderBy('name','asc')->findByField('state_id',$request->id)->pluck('name', 'id')->prepend(trans('lead.select_city'),'');
    }

    private function generateParams()
    {
        $this->user = $this->getUser();

        $priority = $this->optionRepository->getAll()
            ->where('category', 'priority')
            ->map(function ($title) {
                return [
                    'text' => $title->title,
                    'id' => $title->value,
                ];
            })->pluck('text', 'id')->prepend(trans('lead.priority'),'');

        $titles = $this->optionRepository->getAll()
            ->where('category', 'titles')
            ->map(function ($title) {
                return [
                    'text' => $title->title,
                    'id' => $title->value,
                ];
            })->pluck('text', 'id')->prepend(trans('lead.title'),'');

        $countries = $this->countryRepository->orderBy('name', 'asc')->pluck('name', 'id')->prepend(trans('lead.select_country'),'');

        $salesteams = $this->salesTeamRepository->orderBy('id', 'asc')->pluck('salesteam', 'id')->prepend(trans('dashboard.select_sales_team'),'');

        $functions = $this->optionRepository->getAll()->where( 'category', 'function_type' )
            ->map( function ( $title ) {
                return [
                    'title' => $title->title,
                    'value' => $title->value,
                ];
            } )->pluck( 'title', 'value' )
            ->prepend(trans('lead.function'), '');


        view()->share('priority', $priority);
        view()->share('titles', $titles);
        view()->share('countries', $countries);
        view()->share('salesteams', $salesteams);
        view()->share('functions',$functions);
    }

    public function downloadExcelTemplate() {
        ob_end_clean();
        $path = base_path('resources/excel-templates/leads.xlsx');

        if (file_exists($path)) {
            return response()->download($path);
        }

        return 'File not found!';
    }

    public function getImport() {
        $title = trans( 'lead.newupload' );
        return view( 'user.lead.import', compact( 'title' ) );
    }

    public function postImport( Request $request ) {
        if(! ExcelfileValidator::validate( $request ))
        {
            return response('invalid File or File format', 500);
        }
        $reader = $this->excelRepository->load($request->file('file'));
        $customers = $reader->all()->map( function ( $row ) {
            return [
                'company_name'   => $row->company,
                'company_site'   => $row->company_site,
                'address'        => $row->address,
                'product_name'   => $row->product_name,
                'contact_name'   => $row->client_name,
                'email'          => $row->email,
                'function'       => $row->function,
                'phone'          => $row->phone,
                'mobile'         => $row->mobile,
                'country_id'     => 101,
                'priority'       => $row->priority,
            ];
        });
        $countries = $this->countryRepository->orderBy( "name", "asc" )->all()
            ->map( function ( $country ) {
                return [
                    'text' => $country->name,
                    'id'   => $country->id,
                ];
            });
        $salesteams = $this->salesTeamRepository->orderBy('id', 'asc')
            ->all()->map( function ( $salesteam ) {
                return [
                    'text' => $salesteam->salesteam,
                    'id'   => $salesteam->id,
                ];
            } );
        $functions = $this->optionRepository->getAll()->where( 'category', 'function_type' )
            ->map( function ( $title ) {
                return [
                    'title' => $title->title,
                    'value' => $title->value,
                ];
            } )->pluck( 'title', 'value' );
        $priorities = $this->optionRepository->getAll()->where( 'category', 'priority' )
            ->map( function ( $title ) {
                return [
                    'title' => $title->title,
                    'value' => $title->value,
                ];
            } )->pluck( 'title', 'value' );
        return response()->json(compact('customers','countries','salesteams','functions','priorities'), 200);
    }
    public function postAjaxStore( LeadImportRequest $request ) {
        $user = $this->userRepository->getUser();
        $organization = $this->userRepository->getOrganization();
        $request->merge(['user_id' => $user->id, 'organization_id' => $organization->id]);
        $this->leadRepository->create( $request->except( 'created', 'errors', 'selected' ) );

        return response()->json( [], 200 );
    }
}
