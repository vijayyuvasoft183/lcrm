<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use App\Models\Lead;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Appointment;
use App\Models\Order;
use App\Models\InstallationDate;
use Redirect;

class DepartmentAdminAppointmentController extends Controller
{
    private $userRepository;
    private $organizationRepository;

    /**
     * TaskController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository,
        OrganizationRepository $organizationRepository
    ) {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->organizationRepository = $organizationRepository;

        view()->share('type', 'appointment');
    }

    public function index(){

        $appointments = Appointment::all();
        $usersData = User::where(['role_type' => '2'])->get();
        $orders = Order::all();
        $installers = InstallationDate::all();
    	return view('user.departmentAppointment.appointment',compact('appointments','usersData','orders','installers'));
    }

    public function store(Request $request){
        $appointment = new Appointment;
        $appointment->user_id = $this->getUser()->id;
        $appointment->lead_id = $request->lead_id;
        $appointment->title = $request->appointment_description;
        $appointment->assign_date   = date('Y-m-d h:i:s',strtotime($request->appointment_deadline));
        $appointment->save();
    }
    public function create(){

    }
    public function edit($call){

    }
    public function update(){
        $id = $_GET['id'];
        $assginVal = $_GET['selectVal'];

        $appointments = Appointment::find($id);

        $appointments->assign_id = empty($assginVal) ? 0 : $assginVal;
        $appointments->save();
        return Redirect::to('department-appointment');

    }
    public function delete($call){

    }
}
