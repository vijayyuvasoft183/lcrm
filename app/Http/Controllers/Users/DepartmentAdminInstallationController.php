<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Models\InstallationDate;
use App\Http\Controllers\Controller;
use App\Http\Requests\InstallationDateRequest;
use Redirect;

class DepartmentAdminInstallationController extends Controller
{
    function index(){
    	$title = trans('installation.installer');
    	$installers = InstallationDate::all();
    	//print_r($installers);die;
    	return view('user.departmentInstallationDate.index',compact('title','installers'));
    }
    function create(){
    	$title = trans('installation.create');
    	return view('user.departmentInstallationDate.create',compact('title'));
    }
    function store(InstallationDateRequest $request)
    {
    	$installData = new InstallationDate;
    	$installData->appointment_id = $request->appoint_id;
    	$installData->installation_date = date('Y-m-d h:i:s',strtotime($request->installationDate));
    	$installData->description = $request->description;
    	$installData->save();
    	return Redirect('/installationDate');
    }
    function edit(){

    }
    function update(){

    }
    function delete(){

    }
}
