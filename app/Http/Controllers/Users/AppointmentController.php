<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\UserRepository;
use App\Models\Lead;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Appointment;
use Redirect;
use App\Models\Order;

class AppointmentController extends Controller
{
    private $userRepository;
    private $organizationRepository;
    private $taskRepository;

    /**
     * TaskController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        parent::__construct();

        $this->userRepository = $userRepository;
        view()->share('type', 'appointment');
    }

    public function index(){
        $appointments = Appointment::where('assign_id',$this->getUser()->id)->get();
        /*$orders = Appointment::join('orders', 'appointments.lead_id', '=', 'orders.lead_id')
            ->select('orders.*')
            ->get();*/
        $orders = Order::all();
        //print_r($orders);die;
    	return view('user.appointment.appointment',compact('appointments','orders'));
    }

    public function store(Request $request){
       
    }
    public function create(){

    }
    public function edit($call){

    }

    //update only sale completed data
    public function update(){
        $id = $_GET['id'];
        $sale_completed = $_GET['selectVal'];

        $appointments = Appointment::find($id);

        $appointments->sale_completed = $sale_completed;
        $appointments->save();
        return Redirect::to('appointment');
    }

    //Update only sold data
    public function notSoldUpdate(){
        $id = $_GET['id'];
        $not_sold = $_GET['selecteditem'];

        $appointments = Appointment::find($id);

        $appointments->not_sold = $not_sold;
        $appointments->save();
        return Redirect::to('appointment');
    }

    //update only job quote
    function updateJobQuote(){
        $id = $_GET['id'];
        $quotes = $_GET['quotes'] ; 

        $appointments = Appointment::find($id);

        $appointments->job_quote = $quotes;
        $appointments->save();

        return Redirect::to('appointment');
    }
    public function delete($call){

    }
}
