<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'priority' => 'required',
            'email' => 'required|email',
            'contact_name' => 'required',
            'title' => 'required',
            'country_id' => 'required',
            'phone' => 'required|regex:/^\d{5,15}?$/',
            'mobile' => 'required|regex:/^\d{5,15}?$/',
            'address' => 'required',
            'city' => 'required',
            'post_code' => 'required:max:6',
            'notesckeditor' => 'max:1000',
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $this->merge(['tags' => implode(',', $this->get('tags', []))]);

        return parent::getValidatorInstance();
    }

    public function messages()
    {
        return [
            'phone.regex' => 'Phone number can be only numbers',
            'mobile.regex' => 'Mobile number can be only numbers',
            'fax.regex' => 'Fax number can be only numbers',
        ];
    }
}
