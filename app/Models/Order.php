<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use RevisionableTrait,SoftDeletes;

    protected $guarded = ['id'];
    protected $table = 'orders';
    protected $dates = ['deleted_at'];

}