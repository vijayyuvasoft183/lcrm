<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Venturecraft\Revisionable\RevisionableTrait;


class Appointment extends Model
{
    use SoftDeletes,RevisionableTrait,TransformableTrait;

    protected $guarded = ['id'];
    protected $table = 'appointments';
    protected $dates = ['deleted_at'];

    public function getLead(){
    	return $this->belongsTo('App\Models\Lead','lead_id','id');
    }
    public function getUserName(){
    	return $this->belongsTo('App\Models\User','user_id','id');
    }
}