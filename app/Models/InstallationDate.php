<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Venturecraft\Revisionable\RevisionableTrait;


class InstallationDate extends Model
{
    use SoftDeletes,RevisionableTrait,TransformableTrait;

    protected $guarded = ['id'];
    protected $table = 'installation_date';
    protected $dates = ['deleted_at'];

}